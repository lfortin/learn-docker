const express = require("express");
const authController = require("../controllers/authController");

const router = express.Router();

router.route("/signup").post(authController.signUp);
router.route("/signin/:id").post(authController.signIn);
router.route("/:id").delete(authController.deleteUser);

module.exports = router;
