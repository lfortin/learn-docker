const Post = require("../models/postModel");

exports.getAllPosts = async (_req, res, _next) => {
  try {
    const posts = await Post.find();
    res.status(200).json({
      status: "success",
      results: posts.length,
      data: { posts },
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.getOnePost = async (req, res, _next) => {
  try {
    const post = await Post.findById(req.params.id);
    res.status(200).json({
      status: "success",
      data: { post },
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.createPost = async (req, res, _next) => {
  try {
    const post = await Post.create(req.body);
    res.status(200).json({
      status: "success",
      data: { post },
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.updatePost = async (req, res, _next) => {
  try {
    const post = await Post.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    res.status(200).json({
      status: "success",
      data: { post },
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.deletePost = async (req, res, _next) => {
  try {
    await Post.findByIdAndDelete(req.params.id);
    res.status(200).json({
      status: "success",
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};
