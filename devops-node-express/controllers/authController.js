const bcrypt = require("bcryptjs");
const User = require("../models/userModel");

exports.signUp = async (req, res) => {
  const { username, password } = req.body;
  try {
    const hashPwd = await bcrypt.hash(password, 12);
    const newUser = await User.create({
      username,
      password: hashPwd,
    });
    req.session.user = newUser;
    res.status(201).json({
      status: "success",
      data: { user: newUser },
    });
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.signIn = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(404).json({
        status: "failure",
        messsage: "User not found",
      });
    }
    const result = await bcrypt.compare(req.body.password, user.password);
    if (result) {
      req.session.user = user;
      res.status(200).json({
        status: "success",
        data: { user },
      });
    } else {
      res.status(403).json({
        status: "failure",
        messsage: "Password not matching",
      });
    }
  } catch (e) {
    res.status(400).json({
      status: "failure",
      error: e,
    });
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (!user) {
      res.status(404).json({
        status: "failure",
        error: "User not found",
      });
    }
    res.status(200).json({
      status: "success",
    });
  } catch (e) {
    console.log("e : ", e);
    res.status(400).json({
      status: "failure",
    });
  }
};
