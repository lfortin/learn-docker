const express = require("express");
const mongoose = require("mongoose");
const redis = require("redis");
const session = require("express-session");
const cors = require("cors");
const {
  MONGO_IP,
  MONGO_PORT,
  MONGO_PWD,
  MONGO_USER,
  REDIS_URL,
  REDIS_PORT,
  SESSION_SECRET,
} = require("./config/config");
const postRouter = require("./routes/postRoutes");
const userRouter = require("./routes/userRoutes");

let RedisStore = require("connect-redis")(session);
let redisClient = redis.createClient({
  host: REDIS_URL,
  port: REDIS_PORT,
});

const app = express();

const mongoUrl = `mongodb://${MONGO_USER}:${MONGO_PWD}@${MONGO_IP}:${MONGO_PORT}/?authSource=admin`;
const connectWitRetry = () => {
  mongoose
    .connect(mongoUrl, {})
    .then(() => console.log("Succesfully connected to DB"))
    .catch((e) => {
      console.log("Error : ", e);
      setTimeout(connectWitRetry, 5000);
    });
};
connectWitRetry();

app.enable("trust proxy");
app.use(cors({}));
app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: SESSION_SECRET,
    cookie: {
      httpOnly: true,
      maxAge: 30000,
      secure: false,
    },
  })
);
app.use(express.json());
app.get("/api/v1", (_req, res) => {
  res.send("<h2>Hi there</h2>");
  console.log("running");
});

app.use("/api/v1/posts", postRouter);
app.use("/api/v1/auth", userRouter);

const port = process.env.PORT || 1456;
app.listen(port, () => console.log(`listening on port ${port}`));
